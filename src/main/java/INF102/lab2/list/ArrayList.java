package INF102.lab2.list;

import java.util.Arrays;

public class ArrayList<T> implements List<T> {

	
	public static final int DEFAULT_CAPACITY = 10;
	
	private int n;
	
	private Object elements[];
	
	public ArrayList() {
		elements = new Object[DEFAULT_CAPACITY];
	}
		
	@Override
	public T get(int index) {
	
		if (index < 0 || index >= n) // n is the number of elements in the list (not the capacity)
			throw new IndexOutOfBoundsException(); // This is a Java exception that is thrown when the index is out of bounds
		
		@SuppressWarnings("unchecked") // This is to avoid a warning from the compiler
		T element = (T) elements[index]; // This is a Java cast that converts elements[index] to type T
		return element; // This is a Java return statement that returns the element
	}
	
	@Override
	public void add(int index, T element) {
		
		if (index < 0 || index > n) // n is the number of elements in the list (not the capacity)
			throw new IndexOutOfBoundsException(); // This is a Java exception that is thrown when the index is out of bounds
		
		if (n == elements.length) // elements.length is the capacity of the list
			elements = Arrays.copyOf(elements, elements.length*2); // This is a Java method that copies the elements array to a new array with double the capacity
		
		for (int i = n; i > index; i--) // This is a Java for loop that iterates from n to index
			elements[i] = elements[i-1]; // This is a Java assignment statement that assigns elements[i-1] to elements[i]
		
		elements[index] = element; // This is a Java assignment statement that assigns element to elements[index]
		n++; // This is a Java statement that increments n by 1

	}
	
	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n*3 + 2);
		str.append("[");
		for (int i = 0; i < n; i++) {
			str.append((T) elements[i]);
			if (i != n-1)
				str.append(", ");
		}
		str.append("]");
		return str.toString();
	}

}