package INF102.lab2.runtime;

public class RuntimeAnswers {
    
    public String taskA() {  
        return "g"; // It iterates n times lineary 
    }

    public String taskB() {
        return "c"; // Dividing by 2. 
                    //"The concept of halving" source 
                    //https://builtin.com/software-engineering-perspectives/nlogn
    }

    public String taskC() {
        return "d"; // the inner loop's time complexity is constant 
                    //with respect to the outer loop, 
                    //so the overall time complexity is determined by the outer loop, 
                    //which runs n times.
    }

    public String taskD() {
        return "b"; // Not sure about this one. 
    }

}
